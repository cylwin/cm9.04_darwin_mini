// WORKING
//    * setting servo PID tuning
//    * adjustment of lean forward based on play speed
//    * insertion of motions if a motion is currently playing
// KNOWN BUGS
//    * the lean foward is not dependent on the play speed, just the current lean forward. should be tied
//    * robot shakes after programming if battery power is on 

// My gigantic dynamixel header file
#define DXL_BUS_SERIAL1 1  //Dynamixel on Serial1(USART1)  <-OpenCM9.04

#include <CM9_BC.h>
#include "HelloRobo_RPM.h"
#include "dynamixel_type.h"

Dynamixel Dxl(DXL_BUS_SERIAL1);
Dynamixel *pDxl = &Dxl;
XL_320 DynamixelType;

// Adjust for each bot
int Offset_Calibration[] = {18,0,0,0,0,0,0,0,0,3,0,0,0,3,-3,3,0,0,0};
// Sensor offset for dynamic adjustment
int Offset_Accelerometer[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
// Sensor offset for dynamic adjustment
int Offset_Gyro[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
// Sensor offset for dynamic adjustment
int Offset_Magnetometer[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
// Other adjustment factor
int Offset_Misc[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

int miscCOGFactorMax = 6;
int miscCOGFactor = 2;
int ledColour = 0;

BioloidController BioCon;
#define UPDATE_RATE 50000
HardwareTimer Timer(1);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void setup()
{
        //pinMode(BOARD_LED_PIN,OUTPUT);
        //pinMode(An_CMpin, INPUT_ANALOG); // Set Analog input for pin A0
        
	// Setup Dynamixel bus, USB and Bluetooth interfaces
	Dxl.begin(3);
	Dxl.setLibNumberTxRxAttempts(3);

	Dxl.writeWord(BROADCAST_ID, DynamixelType.MAX_TORQUE_L, 1023);
	Dxl.writeWord(BROADCAST_ID, DynamixelType.GOAL_TORQUE_L, 1023);
	Dxl.writeWord(BROADCAST_ID, DynamixelType.PUNCH_L, 1023);
	//setServoTuningParameters(BROADCAST_ID);
        Dxl.jointMode(BROADCAST_ID);
	
	SerialUSB.begin();
	Serial2.begin(57600);

	while (1) {
		delay(1000);
		digitalWrite(BOARD_LED_PIN, LOW);
		SerialUSB.print("Send any value to continue...\n");
		delay(1000);
		digitalWrite(BOARD_LED_PIN, HIGH);
		
		if (SerialUSB.available()) {
                        int val = SerialUSB.read();
			if (val == 'I') 
				break;
		}
		if (Serial2.available()) {
			int val = Serial2.read();
			if (val == 'I') 
				break;
		}
	}

	SerialUSB.print("Now starting program\n");
	// Load the RoboPlusMotion_Array from our _RPM header file
	BioCon.RPM_Setup(HelloRobo_RoboPlusMotion_Array);
	//  This gives the motion engine the location of the RPM page file
	// 	Sets up the motion engine (allocates memory and initializes variables)
	//  Loads the servo IDs from the first pose in the first sequence in the array
	SerialUSB.print("Done with motion engine setup.\n");

        //SerialUSB.print("Setting up interrupt based motion control\n");
        //Timer.pause();                                 // Pause the timer while we're configuring it
        //Timer.setPeriod(UPDATE_RATE);                  // Set up period in microseconds
        //Timer.setMode(TIMER_CH1, TIMER_OUTPUT_COMPARE);// Set up an interrupt on channel 1
        //Timer.setCompare(TIMER_CH1, 1);                // Interrupt 1 count after each update
        //Timer.attachInterrupt(TIMER_CH1, motionUpdate);// Set the interrupt call routines
        //Timer.refresh();                               // Refresh the timer's count, prescale, and overflow
        //Timer.resume();                                // Start the timer counting        
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void loop() {
	// Set up the servos as we want them
	setServoTuningParameters(BROADCAST_ID);

        // Heartbeat
	unsigned int heartbeat = millis();

	/// Set bot to an initial/ready position
	SerialUSB.print("loop() started. Going to Ready Position...\n");

	// Load the offsets
	BioCon.loadOffsets(Offset_Calibration);
	BioCon.loadAccelerometer(Offset_Accelerometer);
	BioCon.loadGyroscope(Offset_Gyro);
	BioCon.loadMagnetometer(Offset_Magnetometer);
	BioCon.loadMiscellaneous(Offset_Misc);

	// Load the initial stance motion page
	BioCon.MotionPage(InitialPose);

	while (BioCon.MotionStatus()) {
		delay(1);
		BioCon.Play();

		if ((millis()-heartbeat)>200) {
			heartbeat = 0;
			SerialUSB.print(".");
		}
	}

	SerialUSB.print(" done.\nStarting main loop for user code.\n");

	/// Main loop where all user code occurs
	// Do not modify anthing outside this while loop unless you are very, very
	//  certain of what you are doing.
	// Do not create any infinite loops or >30ms waits within this loop, or the
	//  motion engine will fail to update.
	while(1){     
  		unsigned int rang = 0;

		if (Serial2.available()) { rang = Serial2.read(); }
		else if (SerialUSB.available()) { rang = SerialUSB.read(); }
		
                // ARROW KEY OPTIONS
		if (rang == 'I') { BioCon.MotionPage(InitialPose); }
                else if (rang == 'S') { BioCon.MotionPage(Advance); }
                else if (rang == 'F') { BioCon.MotionPage(FastAdvance); }
                else if (rang == 'Q') { BioCon.MotionPage(FastAdvanceLeft); }
                else if (rang == 'T') { BioCon.MotionPage(FastAdvanceRight); }
		else if (rang == 'B') { BioCon.MotionPage(Reverse); }
		else if (rang == 'L') { insertMotion(LeftTurn); }
		else if (rang == 'R') { insertMotion(RightTurn); }
		else if (rang == 'i') { insertMotion(RightSideStep); }
		else if (rang == 'j') { insertMotion(LeftSideStep); }

                // ACTION KEY OPTIONS
                else if (rang == 'U') { BioCon.MotionPage(GetUp); }
                else if (rang == 'G') { BioCon.MotionPage(Greet1); }
                else if (rang == 'g') { BioCon.MotionPage(Greet2); }
                else if (rang == 'l') { insertMotion(LeftKick); }
                else if (rang == 'r') { insertMotion(RightKick); }
                else if (rang == 'H') { insertMotion(LeftHook); }
                else if (rang == 'h') { insertMotion(RightHook); }
                else if (rang == '|') {
                        ledColour += 1;
                        
                        if (ledColour == 8)
                            ledColour = 0;
                        
			// Toggle LED Colours
			Dxl.writeWord(BROADCAST_ID, DynamixelType.LED, ledColour);
		}
		else if (rang == '+') {
			// Set the misc offsets
			Offset_Misc[13] = (Offset_Misc[13] >= (1)*miscCOGFactorMax) ? ((1)*miscCOGFactorMax)  : (Offset_Misc[13] + miscCOGFactor);
			Offset_Misc[14] = (Offset_Misc[14] <= (-1)*miscCOGFactorMax) ? (-1)*miscCOGFactorMax : Offset_Misc[14] - miscCOGFactor;
			BioCon.loadMiscellaneous(Offset_Misc);

			// Get the current Play Speed
			unsigned int playSpeed = BioCon.getTimeModifier();

			// Increase Play speed
			BioCon.setTimeModifier(playSpeed-10);
    			//SerialUSB.print("Offset_Misc[13]:");
    			//SerialUSB.println(Offset_Misc[13]);
    			//SerialUSB.print("Offset_Misc[14]:");
    			//SerialUSB.println(Offset_Misc[14]);
    			//SerialUSB.print("Setting play speed:");
			//SerialUSB.println(BioCon.getTimeModifier());
		}
		else if (rang == '-') {
			// Set the misc offsets
			Offset_Misc[13] = (Offset_Misc[13] <= 0) ? 0 : Offset_Misc[13] - miscCOGFactor;
			Offset_Misc[14] = (Offset_Misc[14] >= 0) ? 0 : Offset_Misc[14] + miscCOGFactor;
			BioCon.loadMiscellaneous(Offset_Misc);
			
			// Get the current Play Speed
			unsigned int playSpeed = BioCon.getTimeModifier();
			
			// Increase Play speed
			BioCon.setTimeModifier(playSpeed+10);
    			//SerialUSB.print("Offset_Misc[13]:");
    			//SerialUSB.println(Offset_Misc[13]);
    			//SerialUSB.print("Offset_Misc[14]:");
    			//SerialUSB.println(Offset_Misc[14]);
    			//SerialUSB.print("Setting play speed:");
			//SerialUSB.println(BioCon.getTimeModifier());
		}
		else if (rang == '*') {
  			// Reset the time modifier to default
    			BioCon.setTimeModifier(100);
    			SerialUSB.println("Resetting motion speed");
		}
                
                // INVALID/STOP
                else if (rang > 0) { BioCon.MotionPage(InitialPose); }
		
		if ((millis()-heartbeat)>200) {
			heartbeat = millis();
			//SerialUSB.print(".");

			if (BioCon.MotionStatus()) {
				if (BioCon.MotionPage()>0) {
					SerialUSB.print("Playing page: ");
					SerialUSB.println(BioCon.MotionPage());
				}
			}
		}

                BioCon.Play();	// TODO: replace with timer interrupt-based update.
	}
}

void setServoTuningParameters(int servoID) {
	Dxl.writeByte(servoID, DynamixelType.P_GAIN, 64);
	Dxl.writeByte(servoID, DynamixelType.I_GAIN, 0);
	Dxl.writeByte(servoID, DynamixelType.D_GAIN, 0);
}

void insertMotion(unsigned int motionNumber) {
	// Check currently playing motion
	unsigned int currentMotionNumber = BioCon.MotionPage();
	
	BioCon.MotionPage(motionNumber);
	
	// Wait till the motion is done
	while(BioCon.MotionStatus()) { BioCon.Play(); }
	
	BioCon.MotionPage(currentMotionNumber);
}

void motionUpdate(void) {
        //SerialUSB.print("PLAYING");
	BioCon.Play();
}

void updateSensors(void) {
	//Update the joint offsets based on gyro and accelerometer readings
}
