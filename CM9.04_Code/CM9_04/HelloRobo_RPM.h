#ifndef _HELLOROBO_RPM_H_
#define _HELLOROBO_RPM_H_

#include "HelloRobo_Sequences.h"

// Motion file: HelloRobo_DARWIN-MINI.mtnx

static const unsigned int InitialPose = 1;        // I
static const unsigned int GetUp = 2;              // U
static const unsigned int Greet1 = 5;             // G
static const unsigned int Greet2 = 6;             // g
static const unsigned int RightTurn = 15;         // R
static const unsigned int LeftTurn = 16;          // L
static const unsigned int RightSideStep = 17;     // i
static const unsigned int LeftSideStep = 18;      // j
static const unsigned int Advance = 19;           // S
static const unsigned int Reverse = 22;           // B
static const unsigned int LeftKick = 28;          // l
static const unsigned int RightKick = 29;         // r
static const unsigned int LeftHook = 11;          // H
static const unsigned int RightHook = 8;          // h
static const unsigned int FastAdvance = 32;       // F
static const unsigned int FastAdvanceRight = 34;  // T
static const unsigned int FastAdvanceLeft = 33;   // Q

rpm_page_t HelloRobo_RoboPlusMotion_Array[] __FLASH__ = {
	{0,						0,		37},	// 0
	{HelloRobo_Initial_Position_1,		0,		0},	 // 1
	{HelloRobo_Get_Up_2,		0,		0},	 // 2
	{HelloRobo_Sit_3,		0,		0},	 // 3
	{HelloRobo_Stand_Up_4,		0,		0},	 // 4
	{HelloRobo_Greet1_5,		0,		0},	 // 5
	{HelloRobo_Greet2_6,		0,		0},	 // 6
	{HelloRobo_Right_Jab_7,		0,		0},	 // 7
	{HelloRobo_Right_Hook_8,		0,		0},	 // 8
	{HelloRobo_Right_Uppercut_9,		0,		0},	 // 9
	{HelloRobo_Left_Jab_10,		0,		0},	 // 10
	{HelloRobo_Left_Hook_11,		0,		0},	 // 11
	{HelloRobo_Left_Uppercut_12,		0,		0},	 // 12
	{HelloRobo_Right_Hand_Wave_13,		0,		0},	 // 13
	{HelloRobo_Left_Hand_Wave_14,		0,		0},	 // 14
	{HelloRobo_Right_Turn_15,		0,		0},	 // 15
	{HelloRobo_Left_Turn_16,		0,		0},	 // 16
	{HelloRobo_Sidestep_Right_17,		0,		0},	 // 17
	{HelloRobo_Sidestep_Left_18,		0,		0},	 // 18
	{HelloRobo_Advance_19,		21,		0},	 // 19
	{HelloRobo_Slow_Advance_LFT_20,		0,		0},	 // 20
	{HelloRobo_Advance_2_20_21,		0,		0},	 // 21
	{HelloRobo_Reverse_22,		23,		0},	 // 22
	{HelloRobo_Reverse_2_22_23,		0,		0},	 // 23
	{HelloRobo_Front_Roll_24,		25,		0},	 // 24
	{HelloRobo_Front_Roll_2_24_25,		0,		0},	 // 25
	{HelloRobo_Back_Roll_26,		27,		0},	 // 26
	{HelloRobo_Back_Roll_2_32_27,		0,		0},	 // 27
	{HelloRobo_Left_Kick_28,		0,		0},	 // 28
	{HelloRobo_Right_Kick_29,		0,		0},	 // 29
	{HelloRobo_Left_Side_Kick_30,		0,		0},	 // 30
	{HelloRobo_Right_Side_Kick_31,		0,		0},	 // 31
	{HelloRobo_Fast_Advance_32,		32,		35},	 // 32
	{HelloRobo_Fast_Advance_LEFT_33,		33,		36},	 // 33
	{HelloRobo_Fast_Advance_RIGHT_34,		34,		37},	 // 34
	{HelloRobo_Fast_Advance_2_35,		0,		0},	 // 35
	{HelloRobo_Fast_Advance_2_LEF_36,		0,		0},	 // 36
	{HelloRobo_Fast_Advance_2_RIG_37,		0,		0},	 // 37
};

#endif

