#ifndef _HELLOROBO_SEQUENCES_H_
#define _HELLOROBO_SEQUENCES_H_

#include "HelloRobo_Poses.h"

// Motion file: HelloRobo_DARWIN-MINI.mtnx

bc_seq_t HelloRobo_Initial_Position_1[] __FLASH__ = {{HelloRobo_id,1},{HelloRobo_Initial_Position_1_1,390}};

bc_seq_t HelloRobo_Get_Up_2[] __FLASH__ = {{HelloRobo_id,9},{HelloRobo_Get_Up_2_1,195},{HelloRobo_Get_Up_2_2,390},{HelloRobo_Get_Up_2_3,390},{HelloRobo_Get_Up_2_4,195},{HelloRobo_Get_Up_2_5,195},{HelloRobo_Get_Up_2_6,390},{HelloRobo_Get_Up_2_7,390},{HelloRobo_Get_Up_2_8,390},{HelloRobo_Get_Up_2_9,585}};

bc_seq_t HelloRobo_Sit_3[] __FLASH__ = {{HelloRobo_id,1},{HelloRobo_Sit_3_1,585}};

bc_seq_t HelloRobo_Stand_Up_4[] __FLASH__ = {{HelloRobo_id,1},{HelloRobo_Stand_Up_4_1,585}};

bc_seq_t HelloRobo_Greet1_5[] __FLASH__ = {{HelloRobo_id,3},{HelloRobo_Greet1_5_1,585},{HelloRobo_Greet1_5_2,976},{HelloRobo_Greet1_5_3,585}};

bc_seq_t HelloRobo_Greet2_6[] __FLASH__ = {{HelloRobo_id,6},{HelloRobo_Greet2_6_1,195},{HelloRobo_Greet2_6_2,390},{HelloRobo_Greet2_6_3,585},{HelloRobo_Greet2_6_4,976},{HelloRobo_Greet2_6_5,585},{HelloRobo_Greet2_6_6,390}};

bc_seq_t HelloRobo_Right_Jab_7[] __FLASH__ = {{HelloRobo_id,2},{HelloRobo_Right_Jab_7_1,195},{HelloRobo_Right_Jab_7_2,195}};

bc_seq_t HelloRobo_Right_Hook_8[] __FLASH__ = {{HelloRobo_id,2},{HelloRobo_Right_Hook_8_1,390},{HelloRobo_Right_Hook_8_2,195}};

bc_seq_t HelloRobo_Right_Uppercut_9[] __FLASH__ = {{HelloRobo_id,2},{HelloRobo_Right_Uppercut_9_1,390},{HelloRobo_Right_Uppercut_9_2,195}};

bc_seq_t HelloRobo_Left_Jab_10[] __FLASH__ = {{HelloRobo_id,2},{HelloRobo_Left_Jab_10_1,195},{HelloRobo_Left_Jab_10_2,195}};

bc_seq_t HelloRobo_Left_Hook_11[] __FLASH__ = {{HelloRobo_id,2},{HelloRobo_Left_Hook_11_1,390},{HelloRobo_Left_Hook_11_2,195}};

bc_seq_t HelloRobo_Left_Uppercut_12[] __FLASH__ = {{HelloRobo_id,2},{HelloRobo_Left_Uppercut_12_1,390},{HelloRobo_Left_Uppercut_12_2,195}};

bc_seq_t HelloRobo_Right_Hand_Wave_13[] __FLASH__ = {{HelloRobo_id,6},{HelloRobo_Right_Hand_Wave_13_1,390},{HelloRobo_Right_Hand_Wave_13_2,195},{HelloRobo_Right_Hand_Wave_13_3,195},{HelloRobo_Right_Hand_Wave_13_4,195},{HelloRobo_Right_Hand_Wave_13_5,195},{HelloRobo_Right_Hand_Wave_13_6,976}};

bc_seq_t HelloRobo_Left_Hand_Wave_14[] __FLASH__ = {{HelloRobo_id,6},{HelloRobo_Left_Hand_Wave_14_1,390},{HelloRobo_Left_Hand_Wave_14_2,195},{HelloRobo_Left_Hand_Wave_14_3,195},{HelloRobo_Left_Hand_Wave_14_4,195},{HelloRobo_Left_Hand_Wave_14_5,195},{HelloRobo_Left_Hand_Wave_14_6,976}};

bc_seq_t HelloRobo_Right_Turn_15[] __FLASH__ = {{HelloRobo_id,3},{HelloRobo_Right_Turn_15_1,63},{HelloRobo_Right_Turn_15_2,63},{HelloRobo_Right_Turn_15_3,127}};

bc_seq_t HelloRobo_Left_Turn_16[] __FLASH__ = {{HelloRobo_id,3},{HelloRobo_Left_Turn_16_1,63},{HelloRobo_Left_Turn_16_2,63},{HelloRobo_Left_Turn_16_3,127}};

bc_seq_t HelloRobo_Sidestep_Right_17[] __FLASH__ = {{HelloRobo_id,4},{HelloRobo_Sidestep_Right_17_1,93},{HelloRobo_Sidestep_Right_17_2,7},{HelloRobo_Sidestep_Right_17_3,70},{HelloRobo_Sidestep_Right_17_4,23}};

bc_seq_t HelloRobo_Sidestep_Left_18[] __FLASH__ = {{HelloRobo_id,4},{HelloRobo_Sidestep_Left_18_1,93},{HelloRobo_Sidestep_Left_18_2,7},{HelloRobo_Sidestep_Left_18_3,70},{HelloRobo_Sidestep_Left_18_4,39}};

bc_seq_t HelloRobo_Advance_19[] __FLASH__ = {{HelloRobo_id,8},{HelloRobo_Advance_19_1,70},{HelloRobo_Advance_19_2,7},{HelloRobo_Advance_19_3,70},{HelloRobo_Advance_19_4,70},{HelloRobo_Advance_19_5,70},{HelloRobo_Advance_19_6,7},{HelloRobo_Advance_19_7,70},{HelloRobo_Advance_19_8,70}};

bc_seq_t HelloRobo_Slow_Advance_LFT_20[] __FLASH__ = {{HelloRobo_id,8},{HelloRobo_Slow_Advance_LFT_20_1,195},{HelloRobo_Slow_Advance_LFT_20_2,7},{HelloRobo_Slow_Advance_LFT_20_3,195},{HelloRobo_Slow_Advance_LFT_20_4,203},{HelloRobo_Slow_Advance_LFT_20_5,187},{HelloRobo_Slow_Advance_LFT_20_6,7},{HelloRobo_Slow_Advance_LFT_20_7,203},{HelloRobo_Slow_Advance_LFT_20_8,203}};

bc_seq_t HelloRobo_Advance_2_20_21[] __FLASH__ = {{HelloRobo_id,4},{HelloRobo_Advance_2_20_21_1,70},{HelloRobo_Advance_2_20_21_2,7},{HelloRobo_Advance_2_20_21_3,70},{HelloRobo_Advance_2_20_21_4,70}};

bc_seq_t HelloRobo_Reverse_22[] __FLASH__ = {{HelloRobo_id,6},{HelloRobo_Reverse_22_1,70},{HelloRobo_Reverse_22_2,70},{HelloRobo_Reverse_22_3,70},{HelloRobo_Reverse_22_4,70},{HelloRobo_Reverse_22_5,70},{HelloRobo_Reverse_22_6,70}};

bc_seq_t HelloRobo_Reverse_2_22_23[] __FLASH__ = {{HelloRobo_id,4},{HelloRobo_Reverse_2_22_23_1,70},{HelloRobo_Reverse_2_22_23_2,7},{HelloRobo_Reverse_2_22_23_3,70},{HelloRobo_Reverse_2_22_23_4,70}};

bc_seq_t HelloRobo_Front_Roll_24[] __FLASH__ = {{HelloRobo_id,5},{HelloRobo_Front_Roll_24_1,976},{HelloRobo_Front_Roll_24_2,195},{HelloRobo_Front_Roll_24_3,390},{HelloRobo_Front_Roll_24_4,390},{HelloRobo_Front_Roll_24_5,585}};

bc_seq_t HelloRobo_Front_Roll_2_24_25[] __FLASH__ = {{HelloRobo_id,6},{HelloRobo_Front_Roll_2_24_25_1,390},{HelloRobo_Front_Roll_2_24_25_2,93},{HelloRobo_Front_Roll_2_24_25_3,195},{HelloRobo_Front_Roll_2_24_25_4,390},{HelloRobo_Front_Roll_2_24_25_5,390},{HelloRobo_Front_Roll_2_24_25_6,781}};

bc_seq_t HelloRobo_Back_Roll_26[] __FLASH__ = {{HelloRobo_id,4},{HelloRobo_Back_Roll_26_1,976},{HelloRobo_Back_Roll_26_2,390},{HelloRobo_Back_Roll_26_3,976},{HelloRobo_Back_Roll_26_4,976}};

bc_seq_t HelloRobo_Back_Roll_2_32_27[] __FLASH__ = {{HelloRobo_id,5},{HelloRobo_Back_Roll_2_32_27_1,976},{HelloRobo_Back_Roll_2_32_27_2,390},{HelloRobo_Back_Roll_2_32_27_3,195},{HelloRobo_Back_Roll_2_32_27_4,195},{HelloRobo_Back_Roll_2_32_27_5,781}};

bc_seq_t HelloRobo_Left_Kick_28[] __FLASH__ = {{HelloRobo_id,7},{HelloRobo_Left_Kick_28_1,70},{HelloRobo_Left_Kick_28_2,70},{HelloRobo_Left_Kick_28_3,93},{HelloRobo_Left_Kick_28_4,70},{HelloRobo_Left_Kick_28_5,390},{HelloRobo_Left_Kick_28_6,289},{HelloRobo_Left_Kick_28_7,195}};

bc_seq_t HelloRobo_Right_Kick_29[] __FLASH__ = {{HelloRobo_id,7},{HelloRobo_Right_Kick_29_1,70},{HelloRobo_Right_Kick_29_2,70},{HelloRobo_Right_Kick_29_3,93},{HelloRobo_Right_Kick_29_4,70},{HelloRobo_Right_Kick_29_5,390},{HelloRobo_Right_Kick_29_6,289},{HelloRobo_Right_Kick_29_7,195}};

bc_seq_t HelloRobo_Left_Side_Kick_30[] __FLASH__ = {{HelloRobo_id,5},{HelloRobo_Left_Side_Kick_30_1,70},{HelloRobo_Left_Side_Kick_30_2,70},{HelloRobo_Left_Side_Kick_30_3,195},{HelloRobo_Left_Side_Kick_30_4,390},{HelloRobo_Left_Side_Kick_30_5,195}};

bc_seq_t HelloRobo_Right_Side_Kick_31[] __FLASH__ = {{HelloRobo_id,5},{HelloRobo_Right_Side_Kick_31_1,70},{HelloRobo_Right_Side_Kick_31_2,70},{HelloRobo_Right_Side_Kick_31_3,195},{HelloRobo_Right_Side_Kick_31_4,390},{HelloRobo_Right_Side_Kick_31_5,195}};

bc_seq_t HelloRobo_Fast_Advance_32[] __FLASH__ = {{HelloRobo_id,6},{HelloRobo_Fast_Advance_32_1,35},{HelloRobo_Fast_Advance_32_2,46},{HelloRobo_Fast_Advance_32_3,46},{HelloRobo_Fast_Advance_32_4,35},{HelloRobo_Fast_Advance_32_5,46},{HelloRobo_Fast_Advance_32_6,46}};

bc_seq_t HelloRobo_Fast_Advance_LEFT_33[] __FLASH__ = {{HelloRobo_id,6},{HelloRobo_Fast_Advance_LEFT_33_1,35},{HelloRobo_Fast_Advance_LEFT_33_2,46},{HelloRobo_Fast_Advance_LEFT_33_3,46},{HelloRobo_Fast_Advance_LEFT_33_4,35},{HelloRobo_Fast_Advance_LEFT_33_5,46},{HelloRobo_Fast_Advance_LEFT_33_6,46}};

bc_seq_t HelloRobo_Fast_Advance_RIGHT_34[] __FLASH__ = {{HelloRobo_id,6},{HelloRobo_Fast_Advance_RIGHT_34_1,35},{HelloRobo_Fast_Advance_RIGHT_34_2,46},{HelloRobo_Fast_Advance_RIGHT_34_3,46},{HelloRobo_Fast_Advance_RIGHT_34_4,35},{HelloRobo_Fast_Advance_RIGHT_34_5,46},{HelloRobo_Fast_Advance_RIGHT_34_6,46}};

bc_seq_t HelloRobo_Fast_Advance_2_35[] __FLASH__ = {{HelloRobo_id,3},{HelloRobo_Fast_Advance_2_35_1,70},{HelloRobo_Fast_Advance_2_35_2,70},{HelloRobo_Fast_Advance_2_35_3,70}};

bc_seq_t HelloRobo_Fast_Advance_2_LEF_36[] __FLASH__ = {{HelloRobo_id,3},{HelloRobo_Fast_Advance_2_LEF_36_1,70},{HelloRobo_Fast_Advance_2_LEF_36_2,70},{HelloRobo_Fast_Advance_2_LEF_36_3,70}};

bc_seq_t HelloRobo_Fast_Advance_2_RIG_37[] __FLASH__ = {{HelloRobo_id,3},{HelloRobo_Fast_Advance_2_RIG_37_1,70},{HelloRobo_Fast_Advance_2_RIG_37_2,70},{HelloRobo_Fast_Advance_2_RIG_37_3,70}};

#endif

