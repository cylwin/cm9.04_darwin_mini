//#include "I2C.h"

const uint8_t MPU6050 = 0x68;  // If AD0 is logic low on the PCB the address is 0x68, otherwise set this to 0x69
const uint8_t HMC5883L = 0x1E; // Address of magnetometer
static const uint8_t I2C_TIMEOUT = 100; // Used to check for errors in I2C communication

/* IMU Data */
double accX, accY, accZ;
double gyroX, gyroY, gyroZ;
double magX, magY, magZ;
int16 tempRaw;
uint32 timer;
uint8_t i2cData[14]; // Buffer for I2C data
double accelData[3];
double gyroData[3];
double magData[3];

uint8_t i2cWrite(uint8_t address, uint8_t registerAddress, uint8_t *data, uint8_t length, bool sendStop);

uint8_t i2cWrite(uint8_t address, uint8_t registerAddress, uint8_t data, bool sendStop) {
  return i2cWrite(address, registerAddress, &data, 1, sendStop);      // Returns 0 on success
}

uint8_t i2cWrite(uint8_t address, uint8_t registerAddress, uint8_t *data, uint8_t length, bool sendStop) {
  Wire.beginTransmission(address);
  Wire.write(registerAddress);
  Wire.write(data, length);
  
  uint8_t rcode = Wire.endTransmission(); // Returns 0 on success
  
  if (rcode) {
    SerialUSB.print("i2cWrite failed: "); SerialUSB.println(rcode);
  }
  return rcode;
}

uint8_t i2cRead(uint8_t address, uint8_t registerAddress, uint8_t *data, uint8_t nbytes) {
  uint32 timeOutTimer;
  Wire.beginTransmission(address);
  Wire.write(registerAddress);
  Wire.endTransmission(); // Don't release the bus
  
  Wire.beginTransmission(address);
  Wire.requestFrom(address, nbytes); // Send a repeated start and then release the bus after reading
  for (uint8 i = 0; i < nbytes; i++) {
    if (Wire.available())
      data[i] = Wire.read();
    else {
      timeOutTimer = micros();
      while (((micros() - timeOutTimer) < I2C_TIMEOUT) && !Wire.available());
      if (Wire.available())
        data[i] = Wire.read();
      else {
        SerialUSB.println("i2cRead timeout");
        return 5; // This error value is not already taken by endTransmission
      }
    }
  }
  
  return 0; // Success
}

void setupMPU6050() {
  SerialUSB.println("Set Up IMU");
  delay(100); // Wait for sensors to get ready

  //SerialUSB.begin();
  Wire.begin(25,24);    //i2c bus init SDA->25, SCL->24
  //TWBR = ((F_CPU / 400000L) - 16) / 2; // Set I2C frequency to 400kHz

  i2cData[0] = 7; // Set the sample rate to 1000Hz - 8kHz/(7+1) = 1000Hz
  i2cData[1] = 0x00; // Disable FSYNC and set 260 Hz Acc filtering, 256 Hz Gyro filtering, 8 KHz sampling
  i2cData[2] = 0x00; // Set Gyro Full Scale Range to ±250deg/s
  i2cData[3] = 0x00; // Set Accelerometer Full Scale Range to ±2g
  while (i2cWrite(MPU6050, 0x19, i2cData, 4, false)); // Write to all four registers at once
  while (i2cWrite(MPU6050, 0x6B, 0x01, true)); // PLL with X axis gyroscope reference and disable sleep mode

  while (i2cRead(MPU6050, 0x75, i2cData, 1));
  if (i2cData[0] != 0x68) { // Read "WHO_AM_I" register
    SerialUSB.println("Read WHO_AM_I register");
    while (1) {
      SerialUSB.print("Error reading sensor:");SerialUSB.println(i2cData[0]);
      delay(500);
    }
  }
  
  //delay(100); // Wait for sensors to stabilize
  //timer = micros(); // Initialize the timer
}

uint8_t* geti2cData()  { return i2cData; }
double* getAccelData() { return accelData; }
double* getGyroData()  { return gyroData; }
double* getMagData()   { return magData; }
int16 getTempData()    { return tempRaw; }

void updateMPU6050() {
  while (i2cRead(MPU6050, 0x3B, i2cData, 14)); // Get accelerometer and gyroscope values
  accX = ((i2cData[0] << 8) | i2cData[1]);
  //accX = word((byte)i2cData[1], (byte)i2cData[0]);
  accY = -((i2cData[2] << 8) | i2cData[3]);
  //accY = word((byte)i2cData[2], (byte)i2cData[3]);
  accZ = ((i2cData[4] << 8) | i2cData[5]);
  //accZ = word((byte)i2cData[4], (byte)i2cData[5]);
  tempRaw = (i2cData[6] << 8) | i2cData[7];
  //tempRaw = word((byte)i2cData[6], (byte)i2cData[7]);
  gyroX = -(i2cData[8] << 8) | i2cData[9];
  //gyroX = word((byte)i2cData[8], (byte)i2cData[3]);
  gyroY = (i2cData[10] << 8) | i2cData[11];
  //gyroY = word((byte)i2cData[10], (byte)i2cData[11]);
  gyroZ = -(i2cData[12] << 8) | i2cData[13];
  //gyroZ = word((byte)i2cData[12], (byte)i2cData[13]);
  
  /* Print Data */
  //SerialUSB.print(accX / 16384.0); SerialUSB.print("\t"); // Converted into g's
  SerialUSB.print(accY / 16384.0); SerialUSB.print("\t");
  //SerialUSB.print(accZ / 16384.0); SerialUSB.print("\t");
  
  //SerialUSB.print(gyroX); SerialUSB.print("\t"); // Converted into degress per second
  SerialUSB.print(gyroY); SerialUSB.print("\t");
  //SerialUSB.print(gyroZ); SerialUSB.print("\t");
  SerialUSB.println();
}

void updateHMC5883L() {
  while (i2cRead(HMC5883L, 0x03, i2cData, 6)); // Get magnetometer values
  //magX = ((i2cData[0] << 8) | i2cData[1]);
  magData[0] = ((i2cData[0] << 8) | i2cData[1]);
  //magZ = ((i2cData[2] << 8) | i2cData[3]);
  magData[2] = ((i2cData[2] << 8) | i2cData[3]);
  //magY = ((i2cData[4] << 8) | i2cData[5]);
  magData[1] = ((i2cData[4] << 8) | i2cData[5]);
}
