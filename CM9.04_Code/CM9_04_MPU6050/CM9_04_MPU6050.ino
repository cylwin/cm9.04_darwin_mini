// WORKING
//    * setting servo PID tuning
//    * adjustment of lean forward based on play speed
//    * insertion of motions if a motion is currently playing
// KNOWN BUGS
//    * the lean foward is not dependent on the play speed, just the current lean forward. should be tied
//    * robot shakes after programming if battery power is on 

// My gigantic dynamixel header file
#define DXL_BUS_SERIAL1 1  //Dynamixel on Serial1(USART1)  <-OpenCM9.04
#define XL320

#define USB 0
#define BLUETOOTH 1

#include <CM9_BC.h>
#include "HelloRobo_RPM.h"
#include "dynamixel_type.h"
#include <Wire.h>
#include "MPU6050.h"
#include "InverseKinematicsControl.cpp"


Dynamixel Dxl(DXL_BUS_SERIAL1);
Dynamixel *pDxl = &Dxl;

// Other adjustment factor
int Offset_Calibration[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int Offset_Misc[] = {18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

int miscCOGFactorMax = 6;
int miscCOGFactor = 2;
int ledColour = 0;

BioloidController Darwin;
#define UPDATE_RATE_SENSOR 50000
HardwareTimer TimerSensors(4);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void setup()
{
  //pinMode(BOARD_LED_PIN,OUTPUT);
  //pinMode(An_CMpin, INPUT_ANALOG); // Set Analog input for pin A0

  // Setup Dynamixel bus, USB and Bluetooth interfaces
  Dxl.begin(3);
  Dxl.setLibNumberTxRxAttempts(3);

  Dxl.writeWord(BROADCAST_ID, MAX_TORQUE_L, 1023);
  Dxl.writeWord(BROADCAST_ID, GOAL_TORQUE_L, 1023);
  Dxl.writeWord(BROADCAST_ID, PUNCH_L, 1023);
  //setServoTuningParameters(BROADCAST_ID);
  Dxl.jointMode(BROADCAST_ID);

  SerialUSB.begin();
  Serial2.begin(57600);

  while (1) {
    delay(1000);
    digitalWrite(BOARD_LED_PIN, LOW);
    //SerialUSB.print("Send any value to continue...\n");
    printMessage("Send any value to continue...\n");
    delay(1000);
    digitalWrite(BOARD_LED_PIN, HIGH);

    if (SerialUSB.available()) {
      int val = SerialUSB.read();
      if (val == 'I') 
        break;
    }
    if (Serial2.available()) {
      int val = Serial2.read();
      if (val == 'I') 
        break;
    }
  }

  //SerialUSB.print("Now starting program\n");
  printMessage("Now starting program\n");
  // Load the RoboPlusMotion_Array from our _RPM header file
  Darwin.RPM_Setup(HelloRobo_RoboPlusMotion_Array);
  //  This gives the motion engine the location of the RPM page file
  // 	Sets up the motion engine (allocates memory and initializes variables)
  //  Loads the servo IDs from the first pose in the first sequence in the array
  //SerialUSB.print("Done with motion engine setup.\n");
  printMessage("Done with motion engine setup.\n");
  
  // Set up the IMU
  //Wire.begin(25,24);    //i2c bus init SDA->25, SCL->24
  setupMPU6050();

  //SerialUSB.print("Setting up interrupt based motion control\n");
  printMessage("Setting up interrupt based motion control\n");
  TimerSensors.pause();                                 // Pause the timer while we're configuring it
  TimerSensors.setPeriod(UPDATE_RATE_SENSOR);                  // Set up period in microseconds
  TimerSensors.setMode(TIMER_CH4, TIMER_OUTPUT_COMPARE);// Set up an interrupt on channel 1
  TimerSensors.setCompare(TIMER_CH4, 1);                // Interrupt 1 count after each update
  TimerSensors.attachInterrupt(TIMER_CH4, updateSensors);// Set the interrupt call routines
  TimerSensors.refresh();                               // Refresh the timer's count, prescale, and overflow
  TimerSensors.resume();                                // Start the timer counting
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void loop() {
  // Set up the servos as we want them
  setServoTuningParameters(BROADCAST_ID);

  // Heartbeat
  unsigned int heartbeat = millis();

  /// Set bot to an initial/ready position
  //SerialUSB.print("loop() started. Going to Ready Position...\n");
  printMessage("loop() started. Going to Ready Position...\n");

  // Load the offsets
  Darwin.loadOffsets(Offset_Calibration);
  //Darwin.loadMiscellaneous(Offset_Misc);

  // Load the initial stance motion page
  Darwin.MotionPage(InitialPose);

  while (Darwin.MotionStatus()) {
    delay(1);
    Darwin.Play();

    if ((millis()-heartbeat)>200) {
      heartbeat = 0;
      //SerialUSB.print(".");
      printMessage(".");
    }
  }

  //SerialUSB.print(" done.\nStarting main loop for user code.\n");
  printMessage(" done.\nStarting main loop for user code.\n");

  /// Main loop where all user code occurs
  // Do not modify anthing outside this while loop unless you are very, very
  //  certain of what you are doing.
  // Do not create any infinite loops or >30ms waits within this loop, or the
  //  motion engine will fail to update.
  while(1){     
    unsigned int rang = 0;

    if (Serial2.available()) { 
      rang = Serial2.read(); 
    }
    else if (SerialUSB.available()) { 
      rang = SerialUSB.read(); 
    }

    // INITIALIZE
    if (rang == 'I') { 
      Darwin.MotionPage(InitialPose); 
    }
    // INVERSE KINEMATICS CONTROL
    else if (rang = '@') {
      
    }
    // ARROW KEY OPTIONS
    else if (rang == 'S') { 
      Darwin.MotionPage(Advance); 
    }
    else if (rang == 'F') { 
      Darwin.MotionPage(FastAdvance); 
    }
    else if (rang == 'Q') { 
      Darwin.MotionPage(FastAdvanceLeft); 
    }
    else if (rang == 'T') { 
      Darwin.MotionPage(FastAdvanceRight); 
    }
    else if (rang == 'B') { 
      Darwin.MotionPage(Reverse); 
    }
    else if (rang == 'L') { 
      insertMotionDefaultSpeed(LeftTurn); 
    }
    else if (rang == 'R') { 
      insertMotionDefaultSpeed(RightTurn); 
    }
    else if (rang == 'i') { 
      insertMotionDefaultSpeed(RightSideStep); 
    }
    else if (rang == 'j') { 
      insertMotionDefaultSpeed(LeftSideStep); 
    }

    // ACTION KEY OPTIONS
    else if (rang == 'U') { 
      insertMotionDefaultSpeed(GetUp); 
    }
    else if (rang == 'G') { 
      insertMotionDefaultSpeed(Greet1); 
    }
    else if (rang == 'g') { 
      insertMotionDefaultSpeed(Greet2); 
    }
    else if (rang == 'l') { 
      insertMotionDefaultSpeed(LeftKick); 
    }
    else if (rang == 'r') { 
      insertMotionDefaultSpeed(RightKick); 
    }
    else if (rang == 'H') { 
      insertMotionDefaultSpeed(LeftHook); 
    }
    else if (rang == 'h') { 
      insertMotionDefaultSpeed(RightHook); 
    }
    else if (rang == '|') {
      ledColour += 1;

      if (ledColour == 8)
        ledColour = 0;

      // Toggle LED Colours
      Dxl.writeWord(BROADCAST_ID, LED, ledColour);
    }
    else if (rang == '+') {
      // Get the current Play Speed
      unsigned int currentPlaySpeed = Darwin.getTimeModifier();
      unsigned int newPlaySpeed = currentPlaySpeed - 10;
      ;

      // 50 <= timeModifier <= 100
      if (newPlaySpeed < 50) {
        Offset_Misc[13] = (int)(100 - 50)/10;
        Offset_Misc[14] = (int)(100 - 50)/(-10);
      }
      else if ((newPlaySpeed >= 50) && (newPlaySpeed <= 100)) {
        Offset_Misc[13] = (int)(100 - newPlaySpeed)/10;
        Offset_Misc[14] = (int)(100 - newPlaySpeed)/(-10);
      }
      else {
        Offset_Misc[13] = 0;
        Offset_Misc[14] = 0;
      }

      // Increase Play speed
      Darwin.setTimeModifier(newPlaySpeed);			

      // Set the misc offsets
      Darwin.loadMiscellaneous(Offset_Misc);
    }
    else if (rang == '-') {
      // Get the current Play Speed
      unsigned int currentPlaySpeed = Darwin.getTimeModifier();
      unsigned int newPlaySpeed = currentPlaySpeed + 10;

      // 50 <= timeModifier <= 100
      if (newPlaySpeed < 50) {
        Offset_Misc[13] = (int)(100 - 50)/(-10);
        Offset_Misc[14] = (int)(100 - 50)/10;
      }
      else if ((newPlaySpeed >= 50) && (newPlaySpeed <= 100)) {
        Offset_Misc[13] = (int)(100 - newPlaySpeed)/(-10);
        Offset_Misc[14] = (int)(100 - newPlaySpeed)/10;
      }
      else {
        Offset_Misc[13] = 0;
        Offset_Misc[14] = 0;
      }

      // Increase Play speed
      Darwin.setTimeModifier(newPlaySpeed);
    }
    else if (rang == '*') {
      // Reset the time modifier to default
      Darwin.setTimeModifier(100);
      //SerialUSB.println("Resetting motion speed");
      printMessage("Resetting motion speed");
    }

    // INVALID/STOP
    else if (rang > 0) { 
      Darwin.MotionPage(InitialPose); 
    }

    if ((millis()-heartbeat)>200) {
      heartbeat = millis();
      //SerialUSB.print(".");

      if (Darwin.MotionStatus()) {
        if (Darwin.MotionPage()>0) {
          //SerialUSB.print("Playing page: ");
          printMessage("Playing page: ");
          //SerialUSB.println(Darwin.MotionPage());
          printMessage(Darwin.MotionPage());
        }
      }
    }

    //Darwin.Play();	// TODO: replace with timer interrupt-based update.
  }
}

void setServoTuningParameters(int servoID) {
  Dxl.writeByte(servoID, P_GAIN, 64);
  Dxl.writeByte(servoID, I_GAIN, 0);
  Dxl.writeByte(servoID, D_GAIN, 0);
}

void insertMotion(unsigned int motionNumber) {
  // Check currently playing motion
  unsigned int currentMotionNumber = Darwin.MotionPage();

  Darwin.MotionPage(motionNumber);

  // Wait till the motion is done
  while(Darwin.MotionStatus()) { 
    Darwin.Play(); 
  }

  Darwin.MotionPage(currentMotionNumber);
}

void insertMotionDefaultSpeed(unsigned int motionNumber) {
  // Check currently playing motion and current play speed
  unsigned int currentMotionNumber = Darwin.MotionPage();
  unsigned int playSpeed = Darwin.getTimeModifier();

  // Set the new motion to play
  Darwin.MotionPage(motionNumber);
  Darwin.setTimeModifier(100);

  // Wait till the new motion is done
  while(Darwin.MotionStatus()) { 
    Darwin.Play(); 
  }

  // Set the motion speed back to the original and play the original motion
  Darwin.setTimeModifier(playSpeed);
  Darwin.MotionPage(currentMotionNumber);
}

void motionUpdate(void) {
  SerialUSB.println("PLAYING");
  Darwin.Play();
}

void updateSensors(void) {
  // Read the sensor values from the IMU
  updateMPU6050();

  //double gyro = *getGyroData();
  //SerialUSB.print("getGyroRate X: ");SerialUSB.println(getGyroData()[0]);
  //SerialUSB.print("getGyroRate Y: ");SerialUSB.println(getGyroData()[1]);
  //SerialUSB.print("getGyroRate Z: ");SerialUSB.println(getGyroData()[2]);

  //Update the joint offsets based on gyro and accelerometer readings	
  //Offset_Gyro[13] = 2;
  //Offset_Gyro[14] = 2;
}

boolean printMessage(char* message) {
  if (USB) {
    SerialUSB.println(message);
    return true;
  }
  else if (BLUETOOTH) {
    Serial2.println(message);
    return true;
  }
  else
    return false;
}

boolean printMessage(int number) {
  if (USB) {
    SerialUSB.println(number);
    return true;
  }
  else if (BLUETOOTH) {
    Serial2.println(number);
    return true;
  }
  else
    return false;
}
