#include <CM9_BC.h>

/// Bioloid Controller Class for CM9 clients.
class InverseKinematicsControl
{
public:
  InverseKinematicsControl() {};
  //void setup(BioloidController);

private:
  unsigned char * id_;
  // Change in each servo position per interpolation step
  int * deltas_;
  // Calibration offsets for each servo
  int * offsets_;
  // Offsets that will be updated regularly for dynamic motions
  int * miscellaneous_;
  // 'Resolution' of each servo
  unsigned int * resolutions_;

  enum
  {
    INTERPOLATING,
    INTERPOLATION_DONE,
    SEQUENCE_DONE
  } seqState_;	
};
