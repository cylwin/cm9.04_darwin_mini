const int MPU6050 = 0x68;  // I2C address of the MPU-6050
const int HMC5883L = 0x1E; // Address of magnetometer

int16 AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
int gyroXOffset = 170;
int gyroYOffset = 170;
int gyroZOffset = 170;
double gyroXRate, gyroYRate, gyroZRate;
double gyroRawToRate = 131.0;
double magX, magY, magZ;

void setupIMU(){
  Wire.beginTransmission(MPU6050);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the IMU)
  Wire.endTransmission();
  
  Wire.beginTransmission(HMC5883L);
  Wire.write(0x00);
  Wire.write(0x02);
  Wire.endTransmission();
}

void updateMPU6050(){
  Wire.beginTransmission(MPU6050);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission();
  Wire.requestFrom(MPU6050,14);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
  
  gyroXRate = GyX / gyroRawToRate;
  gyroYRate = GyY / gyroRawToRate;
  gyroZRate = GyZ / gyroRawToRate;
}

void updateHMC5883L() {
  Wire.beginTransmission(HMC5883L);
  Wire.write(0x03);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission();
  Wire.requestFrom(HMC5883L,6);  // request a total of 14 registers
  magX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
  magY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  magZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
}

int getAccelX() { return AcX; }
int getAccelY() { return AcY; }
int getAccelZ() { return AcZ; }

double getGyroRateX() { return gyroXRate; }
double getGyroRateY() { return gyroYRate; }
double getGyroRateZ() { return gyroZRate; }

double getMagX() { return magX; }
double getMagY() { return magY; }
double getMagZ() { return magZ; }
